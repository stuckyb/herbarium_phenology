This directory contains a comprehensive list (according to the APG publications) of valid angiosperm family names and a large number of synonyms.

Current, valid family names were taken from the [APG IV publication](https://academic.oup.com/botlinnean/article/181/1/1/2416499#84268287).

All family names (valid and synonyms) available from APG II, the most recent edition to include synonymies, are from the [APG II publication](https://academic.oup.com/botlinnean/article/141/4/399/2433548#84152951)

Names and synonymies were extracted programmatically from the web-based version of each publication.

